# Python Template

This is my template for Python projects.

# Features

This template implements following things:

- pyright configuration, to find the right files
- skeleton for [argparse](https://docs.python.org/3/library/argparse.html)
- basic implementation for [configparser](https://docs.python.org/3/library/configparser.html)
- [Python.gitignore](https://github.com/github/gitignore/blob/main/Python.gitignore) from GitHub

# ToDo

- python [logging](https://docs.python.org/3/library/logging.html)
